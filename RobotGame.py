#!/usr/bin/python3
# Take the code that I have written and complete the following
# todo list for extra credit points.

# TODO
# 10pts - Win when the winning spot is reached
# 20pts - Reset the position to the original after every try
# 20pts - LANDMINES!
# 50pts - Implement programming logic (loops, functions, etc.)

import random

def robotMove(direction):
    if direction == "U" and position[1] < grid[1]:
        position[1] += 1
        print("Moved up")
        print(position)
    elif direction == "D" and position[1] > 1:
        position[1] -= 1
        print("Moved down")
        print(position)
    elif direction == "R" and position[0] < grid[1]:
        position[0] += 1
        print("Moved right")
        print(position)
    elif direction == "L" and position[0] > 1:
        position[0] -= 1
        print("Moved left")
        print(position)
    else:
        print("Not a correct direction, kid")

def randomCoord():
    randomCoordinate = [random.randint(1,grid[0]), random.randint(1,grid[1])]
    return randomCoordinate

def main():
    win = 0
    while win == 0:
        directions = []
        userInput = input("Enter your steps: ").split(",")
        if len(userInput) <= 10:
            for i in userInput:
                directions.append(i)

            for j in directions:
                robotMove(j)
        else:
            print("Try to do it in less moves!")


grid = [5,5]
winPostion = randomCoord()
print("Win at: ",winPostion)
position = randomCoord()
print("You are at:", position)
main()
